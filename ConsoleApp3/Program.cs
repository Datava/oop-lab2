﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                    System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            bool ok;
            uint n, k;
            double sum = 0;
            Console.WriteLine("Лабораторна робота №2.\nВиконав: Юрченко О.А., група КІ-3\nВаріант №12.\nЗавдання 3.2.");
            do
            {
                Console.Write("Введіть додатнє значення N = ");
                ok = uint.TryParse(Console.ReadLine(), out n);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення N. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            do
            {
                Console.Write("Введіть додатнє значення K = ");
                ok = uint.TryParse(Console.ReadLine(), out k);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення K. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            for (ushort i = 1; i <= n; i++)
                sum += (Math.Pow(i, k));
            Console.WriteLine($"Результат обчислення: sum = {sum}");
            Console.ReadLine();
        }
    }
}
