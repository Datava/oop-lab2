﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp
{
    public partial class vchkj : Form
    {
        public vchkj()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a, b, c, d, x1, x2;
            bool ok;
            ok = double.TryParse(textBoxA.Text, out a);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення a!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ok = double.TryParse(textBoxB.Text, out b);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення b!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ok = double.TryParse(textBoxC.Text, out c);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення c!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            d = b * b - 4 * a * c;
            textBoxD.Text = d.ToString();
            labelX1.Text = "x1 =";
            labelX1.Visible = false;
            textBoxX1.Visible = false;
            labelX2.Visible = false;
            textBoxX2.Visible = false;
            labelFalse.Visible = false;
            if (d < 0)
                labelFalse.Visible = true;
            else
                if (d == 0)
            {
                x1 = (-b) / (2 * a);
                labelX1.Text = "x =";
                labelX1.Visible = true;
                textBoxX1.Text = x1.ToString();
                textBoxX1.Visible = true;
            }
            else
            {
                x1 = ((-b) + Math.Sqrt(d)) / (2 * a);
                x2 = ((-b) - Math.Sqrt(d)) / (2 * a);
                textBoxX1.Text = x1.ToString();
                textBoxX2.Text = x2.ToString();
                labelX1.Visible = true;
                textBoxX1.Visible = true;
                labelX2.Visible = true;
                textBoxX2.Visible = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
        }
    }
}
