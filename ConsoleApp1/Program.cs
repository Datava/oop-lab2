﻿using System;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            double x, y, z, s, sum;
            bool ok;
            Console.WriteLine("Лабораторна робота №2.\nВиконав: Юрченко О.А., група КІ-3\nВаріант №12.\nЗавдання 1.");
            do
            {
                Console.Write("Введіть дробове значення x = ");
                ok = double.TryParse(Console.ReadLine(), out x);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення x. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            do
            {
                Console.Write("Введіть дробове значення y = ");
                ok = double.TryParse(Console.ReadLine(), out y);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення y. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            do
            {
                Console.Write("Введіть дробове значення z = ");
                ok = double.TryParse(Console.ReadLine(), out z);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення z. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            s = y * (Math.Atan(z) - 1 / 3) / (Math.Abs(x) + 1 / (y * y + 1));
            sum = Math.Pow(2, Math.Pow(y, x)) + Math.Pow(Math.Pow(3, x), y) + s;
            Console.WriteLine($"Результат обчислення: sum = {sum:F3}");
            Console.ReadKey();
        }
    }
}