﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double a, b, c, d, x1, x2;
            bool ok;
            ok = double.TryParse(TextBoxA.Text, out a);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення a!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ok = double.TryParse(TextBoxB.Text, out b);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення b!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error); return;
            }
            ok = double.TryParse(TextBoxC.Text, out c);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення c!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error); return;
            }
            d = b * b - 4 * a * c;
            TextBoxD.Text = d.ToString();
            LabelX1.Content = "x1 =";
            LabelX1.Visibility = Visibility.Hidden;
            TextBoxX1.Visibility = Visibility.Hidden;
            LabelX2.Visibility = Visibility.Hidden;
            TextBoxX2.Visibility = Visibility.Hidden;
            LabelX1.Visibility = Visibility.Hidden;
            LabelFalse.Visibility = Visibility.Hidden;
            if (d < 0)
                LabelFalse.Visibility = Visibility.Visible;
            else
                if (d == 0)
            {
                x1 = (-b) / (2 * a);
                LabelX1.Content = "x =";
                TextBoxX1.Text = x1.ToString();
                TextBoxX1.Visibility = Visibility.Visible;
            }
            else
            {
                x1 = ((-b) + Math.Sqrt(d)) / (2 * a);
                x2 = ((-b) - Math.Sqrt(d)) / (2 * a);
                TextBoxX1.Text = x1.ToString();
                TextBoxX2.Text = x2.ToString();
                LabelX1.Visibility = Visibility.Visible;
                TextBoxX1.Visibility = Visibility.Visible;
                LabelX2.Visibility = Visibility.Visible;
                TextBoxX2.Visibility = Visibility.Visible;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
        }
    }
}
