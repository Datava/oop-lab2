﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            bool ok;
            double sum = 0;
            uint n, a;
            Console.WriteLine("Лабораторна робота №2.\nВиконав: Юрченко О.А., група КІ-3\nВаріант №12.\nЗавдання 3.1.");
            do
            {
                Console.Write("Введіть додатнє значення N = ");
                ok = uint.TryParse(Console.ReadLine(), out n);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення N. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            a = n;
            for (ushort i = 1; i <= a; i++, n--)
                sum += Math.Pow(i, n);
            Console.WriteLine($"Результат обчислення: sum = {sum}");
            Console.ReadLine();
        }
    }
}
