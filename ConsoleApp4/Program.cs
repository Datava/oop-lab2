﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            bool ok;
            int i = 0, p = 0, d = 0;
            int[] num = new int[100];
            Console.WriteLine("Лабораторна робота №2.\nВиконав: Юрченко О.А., група КІ-3\nВаріант №12.\nЗавдання 3.3.");
            do
            {
                i++;
                do
                {
                    Console.Write($"[{i}] = ");
                    ok = int.TryParse(Console.ReadLine(), out num[i]);
                    if (!ok)
                        Console.WriteLine("  Помилка введення значення N. Будь-ласка повторіть введення значення ще раз!");
                } while (!ok);
            } while (num[i] != 0);
            for (int j = 1; j <= i; j++) 
            {
                if ((num[j] > 0) && num[j] != 0)
                    d++;
                if ((num[j] % 2 == 0) && num[j] != 0)
                    p++;
            }            
            Console.WriteLine($"Парних чисел: {p}\nНепарних чисел: {i-p-1}\nДодатніх: {d}\nВід'ємних: {i - d - 1}");
            Console.ReadLine();
        }
    }
}
