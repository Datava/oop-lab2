﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
             System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            double a, b, c, d;
            bool ok;
            Console.WriteLine("Лабораторна робота №2.\nВиконав: Юрченко О.А., група КІ-3\nВаріант №12.\nЗавдання 1.");
            do
            {
                Console.Write("a = ");
                ok = double.TryParse(Console.ReadLine(), out a);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення a. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            do
            {
                Console.Write("b = ");
                ok = double.TryParse(Console.ReadLine(), out b);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення b. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            do
            {
                Console.Write("c = ");
                ok = double.TryParse(Console.ReadLine(), out c);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення c. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            d = b * b - 4 * a * c;
            Console.WriteLine($"\nD = {d}");
            if (d < 0)
                Console.WriteLine("Коренів немає");
            else
                if (d == 0)
                Console.WriteLine($"x = {(-b) / (2 * a)}");
            else
                Console.WriteLine($"x1 = {(-b + Math.Sqrt(d)) / (2 * a)}\nx2 = {(-b - Math.Sqrt(d)) / (2 * a)}");
            Console.ReadKey();
        }
    }
}
